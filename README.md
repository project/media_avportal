# Media AV Portal

[![Build Status](https://drone.fpfis.eu/api/badges/openeuropa/media_avportal/status.svg?branch=8.x-1.x)](https://drone.fpfis.eu/openeuropa/media_avportal)
[![Packagist](https://img.shields.io/packagist/v/openeuropa/media_avportal.svg)](https://packagist.org/packages/openeuropa/media_avportal)

Media AV Portal adds the [European Audiovisual Services](http://ec.europa.eu/avservices/) as a supported media provider.

# Supported media types

Only 3 media assets types from AV Portal are currently supported:

* PHOTO (resources like [https://audiovisual.ec.europa.eu/en/photo/P-038924](https://audiovisual.ec.europa.eu/en/photo/P-038924)).
* VIDEO (resources like [https://audiovisual.ec.europa.eu/en/video/I-183993](https://audiovisual.ec.europa.eu/en/video/I-183993)).
* REPORTAGE (resources like [https://audiovisual.ec.europa.eu/en/photo/P-038924~2F00-15](https://audiovisual.ec.europa.eu/en/photo/P-038924~2F00-15)).

# Mock service

The project also comes with a test module that provides a mock to the remote AV Portal service. Meaning that tests do not have to be run against the remote service but local resources are used.

## How to use the mock

Enable the `media_avportal_mock` inside a given test and all HTTP requests going to AV Portal API will be intercepted automatically. These, then, will return predefined responses that can be inspected in the `responses` folder of the mock module.

By default, there are 3 types of requests that are mocked:

* A default request with no options
* A request for a given resource (the options contain the `ref` key)
* A request that searches for a given term (the options contain the `kwgg` key)

Additionally, any request to a resource thumbnail will return a local thumbnail image.

## Extending the mock

If another module needs to test interactions that require more responses, these can be provided via an event subscriber (to the `AvPortalMockEvent` event).

In the subscriber, 3 types of responses (in JSON format) can be provided:

* Individual resources
* Search results for a given term
* A default response to replace the existing one

As an example, you can see the subscriber that provides the default resources, `AvPortalMockEventSubscriber`.

**Table of contents:**

- [Installation](#installation)
- [Contributing](#contributing)
- [Versioning](#versioning)

## Installation

The recommended way of installing the module is via [Composer][2].

```bash
composer require drupal/media_avportal
```

### Enable the module

In order to enable the module in your project run:

```bash
./vendor/bin/drush en media_avportal -y
```

### Usage with Drupal 10.2

For Drupal 10.2, the drupal/remote_stream_wrapper (issue 3437974) patch needs to be removed.
If you require this module in Drupal 10.2 you must unset the patch in your composer.json.\
You can do this with the following command:
```bash
composer config --merge --json "extra.patches-ignore.drupal/media_avportal" '{"drupal/remote_stream_wrapper": {"Drupal 10.3.x only - see media_avportal/README.md for 10.2.x - https://www.drupal.org/project/remote_stream_wrapper/issues/3437974": "https://www.drupal.org/files/issues/2024-06-21/drupal_10_3_deliver_signature_change-3437974-2_0_0-18.patch"}}'
```

### Using Docker Compose

Alternatively, you can build a development site using [Docker](https://www.docker.com/get-docker) and
[Docker Compose](https://docs.docker.com/compose/) with the provided configuration.

Docker provides the necessary services and tools such as a web server and a database server to get the site running,
regardless of your local host configuration.

### Disable Drupal caching

Manually disabling Drupal caching is a laborious process that is well described [here][10].

Alternatively you can use the following Drupal Console commands to disable/enable Drupal 8 caching:

```bash
./vendor/bin/drupal site:mode dev  # Disable all caches.
./vendor/bin/drupal site:mode prod # Enable all caches.
```

Note: to fully disable Twig caching the following additional manual steps are required:

1. Open `./build/sites/default/services.yml`
2. Set `cache: false` in `twig.config:` property. E.g.:

```yaml
parameters:
 twig.config:
   cache: false
```

3. Rebuild Drupal cache: `./vendor/bin/drush cr`

This is due to the following [Drupal Console issue][11].

## Contributing

Please read [the full documentation](https://www.drupal.org/community/contributor-guide) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the available versions, see the [tags on this repository](https://git.drupalcode.org/project/media_avportal/tags).

[2]: https://www.drupal.org/docs/develop/using-composer/using-composer-to-manage-drupal-site-dependencies#managing-contributed
[3]: https://github.com/openeuropa/task-runner
[4]: https://docs.docker.com/compose
[7]: https://www.drupal.org/project/config_devel
[8]: https://www.docker.com/get-docker
[9]: https://docs.docker.com/compose
[10]: https://www.drupal.org/node/2598914
[11]: https://github.com/hechoendrupal/drupal-console/issues/3854
[12]: https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
