<?php

declare(strict_types=1);

namespace Drupal\media_avportal;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Factory service to instantiate AV Portal clients.
 */
class AvPortalClientFactory implements AvPortalClientFactoryInterface, ContainerInjectionInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A cache backend interface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new AvPortalClientFactory.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   A cache backend interface.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $configFactory, CacheBackendInterface $cache, TimeInterface $time) {
    $this->httpClient = $http_client;
    $this->configFactory = $configFactory;
    $this->cache = $cache;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('config.factory'),
      $container->get('cache.default'),
      $container->get('datetime.time')
    );
  }

  /**
   * Returns a new client instance.
   *
   * @param array $config
   *   An array of configuration options for the client. Available options:
   *   - use_cache: whether or not to cache the AV Portal responses. Defaults
   *     to TRUE.
   *
   * @return \Drupal\media_avportal\AvPortalClientInterface
   *   A new client instance.
   */
  public function getClient(array $config = []): AvPortalClientInterface {
    $default_config = [
      'use_cache' => TRUE,
    ];
    $config = array_merge($default_config, $config);

    return new AvPortalClient(
      $this->httpClient,
      $this->configFactory,
      $this->cache,
      $this->time,
      $config['use_cache']
    );
  }

}
